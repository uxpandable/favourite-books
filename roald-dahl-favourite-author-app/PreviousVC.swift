//
//  DetailVC.swift
//  roald-dahl-favourite-author-app
//
//  Created by Nils Bjarne Gunnufsen on 06/06/2016.
//  Copyright © 2016 N B Gunnufsen. All rights reserved.
//

import UIKit
import WebKit

class PreviousVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var authorOfTheMonth: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    
    var authorImage = ["http://bookriotcom.c.presscdn.com/wp-content/uploads/2013/09/roald-dahl.jpg", "https://tse4.mm.bing.net/th?id=OIP.Me58f6f801161501eedc5ee471bbce172o1&pid=15.1", "http://www.psephizo.com/wp-content/uploads/2015/02/stephenfry.jpg"]
    var authorNames = ["Roald Dahl", "Stephen King", "Stephen Fry"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
       if let cell = tableView.dequeueReusableCellWithIdentifier("AuthorCell") as? AuthorCell {
                var img: UIImage!
        
                let url = NSURL(string: authorImage[indexPath.row])!
                if let data = NSData(contentsOfURL: url){
                    img = UIImage(data: data)
                } else {
                    img = UIImage(named: "roald dahl chair")
                }
        
                cell.configureCell(img, text: authorNames[indexPath.row])
                return cell
       } else {
                return AuthorCell()
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return authorImage.count
    }
    
    
    
    
    override func viewDidAppear(animated: Bool) {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        backBtn.layer.cornerRadius = 4.0
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func backBtnPressed(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }

  
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
