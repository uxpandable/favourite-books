//
//  ViewController.swift
//  roald-dahl-favourite-author-app
//
//  Created by Nils Bjarne Gunnufsen on 05/06/2016.
//  Copyright © 2016 N B Gunnufsen. All rights reserved.
//

import UIKit

class ViewController: UIViewController  {

    
    @IBOutlet weak var moreBtn: UIButton!
    @IBOutlet weak var previousBtn: UIButton!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        moreBtn.layer.cornerRadius = 4.0
        previousBtn.layer.cornerRadius = 4.0

    }


}

