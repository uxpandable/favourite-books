//
//  authorCell.swift
//  roald-dahl-favourite-author-app
//
//  Created by Nils Bjarne Gunnufsen on 15/06/2016.
//  Copyright © 2016 N B Gunnufsen. All rights reserved.
//

import UIKit

class AuthorCell: UITableViewCell {

    
    @IBOutlet weak var mainImg: UIImageView!
    @IBOutlet weak var mainLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        mainImg.layer.cornerRadius = 5.0
        mainImg.clipsToBounds = true
    }

    func configureCell(image: UIImage, text: String){
        mainImg.image = image
        mainLbl.text = text
    }
}
