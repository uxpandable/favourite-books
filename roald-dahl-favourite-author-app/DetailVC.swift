//
//  DetailVC.swift
//  roald-dahl-favourite-author-app
//
//  Created by Nils Bjarne Gunnufsen on 06/06/2016.
//  Copyright © 2016 N B Gunnufsen. All rights reserved.
//

import UIKit
import WebKit

class DetailVC: UIViewController {

    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var container: UIView!
    var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        webView = WKWebView()
        container.addSubview(webView)
      
        
    }

    override func viewDidAppear(animated: Bool) {
        let frame = CGRectMake(0, 0, container.bounds.width, container.bounds.height)
        webView.frame = frame
        
       loadRequest("https://en.wikipedia.org/wiki/Roald_Dahl")

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        backBtn.layer.cornerRadius = 4.0
        // Dispose of any resources that can be recreated.
    }
    
    
    func loadRequest(urlStr: String) {
        let url = NSURL(string: urlStr)!
        let request = NSURLRequest(URL: url)
        webView.loadRequest(request)
    }
    
    @IBAction func backBtnPressed(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func loadWikipedia(sender: AnyObject) {
        loadRequest("https://en.wikipedia.org/wiki/Roald_Dahl")
    }
    
    @IBAction func loadFoyles(sender: AnyObject) {
        loadRequest("http://www.foyles.co.uk/Public/Shop/Search.aspx?quick=true&searchBy=5&term=Roald%20Dahl")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
